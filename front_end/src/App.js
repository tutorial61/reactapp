// import logo from './logo.svg';
import Header from './components/Header';
import Main from './components/Main';
import Sidebar from './components/Sidebar';
// import './App.css';
import './index.css'

function App() {
  return (
    <div className="App">
      <Header />
      <div>
        <Main userName="Thabiso" num ={5}/>
        <Sidebar />
      </div>
      
    </div>
  );
}

export default App;
