import React from 'react';

function Main(props){
    return <>
        <h1>Hello, {props.userName}</h1>
        <h2>You are in position no.{props.num}</h2>
    </> 
    
}

export default Main;