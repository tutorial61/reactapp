import './App.css';

const App = () => {
  return (
    <div className='app'>
      <section className='side-bar'>
        <button>+ New chat</button>
        <ul className='history'>
          <li>History</li>
        </ul>
        <nav>
          <p id="xque">CirtaAI</p>
        </nav>
      </section>
      <section className='main'>
        <h1>CirtaAI</h1>
        <ul className='feed'>

        </ul>

        <div className='bottom-section'>

          <div className='input-container'>
            <input/>
            <div id='submit'></div>
          </div>
          <p className='info'>
            CirtaAI is a trained ChatGPT model. Trained to assist with,
             South African music industry topics & Stats, CirtaAI will assist with various basic task,
             Like drafting a social media post, post it at optimal times,
            gather info from them & provide reports & alerts when milestones are reached,
            CirtaAI will also manage your profile & offer suggestions based on behaivors observed & more as we get more data.   

          </p>

        </div>
        
      </section>

    </div>
  )
}

export default App;
