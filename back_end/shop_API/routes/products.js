// import express 
const express = require('express');
// import ROUTER from express
const router = express.Router();


/* Using the router 
create a request handler 
(eg) router.get('path',(req,res,next)=>{
    add status,message body etc
})
*/
router.get('/',(request,response,next) =>{
    response.status(200).json({
        message:'handling GET request to /products'
    });
});

router.post('/',(request,response,next) =>{
    response.status(201).json({
        message:'handling POST request to /products'
    });
});

// create a route handler with product id (:productId)
router.get('/:productId',(request,response,next) =>{
    // get id from get request path params
    const id = request.params.productId
    if(id === 'sale'){
        response.status(200).json({
        message:'this is a sale product'
    });
    }else{
        response.status(200).json({
        message:`You passed productId ${id}`
    });
    }
});


// create a patch router to update product with product id
router.patch('/:productId',(request,response,next) =>{
    // get id from get request path params
    response.status(200).json({
    message:`Updated product with productId ${request.params.productId}`
    });
 
}); 

// create a patch router to delete product with product id
router.delete('/:productId',(request,response,next) =>{
    // get id from get request path params
    response.status(200).json({
    message:`Deleted product with productId ${request.params.productId}`
    });
 
}); 




// export module
module.exports = router;