// import express 
const express = require('express');
// import ROUTER from express
const router = express.Router();


/* Using the router 
create a request handler 
(eg) router.get('path',(req,res,next)=>{
    add status,message body etc
})
*/
// get all orders
router.get('/',(request,response,next) =>{
    response.status(200).json({
        message:'Orders Fetched'
    });
});

// post an order
router.post('/',(request,response,next) =>{
    response.status(201).json({
        message:'Order Created'});
});

// get order by id 
router.get('/:orderId',(request,response,next) => {
    response.status(200).json({
        message:'Order Fetched',
        orderID:request.params.orderId
    });
});


// delete order by id 
router.delete('/:orderId',(request,response,next) => {
    response.status(200).json({
        message:'Order Deleted',
        orderID:request.params.orderId
    });
});


// patch order by id 
router.patch('/:orderId',(request,response,next) => {
    response.status(200).json({
        message:'Order Modified',
        orderID:request.params.orderId
    });
});





// export module
module.exports = router;