/* this file will handle requests */
// import express
const express = require('express');
// import product routes 
const productsRoutes = require('./routes/products');
const ordersRoutes = require('./routes/orders');
// import morgan to log processes
const morgan = require('morgan');


// then spin/start server
const app = express();


// use morgan to log server processes
app.use(morgan('dev'));


/* Use as a method sets up middleware
an incoming request has to go through 
app.use(), & to what ever we pass through use()
what ever we pass can be in different formats
it can be a function (eg)
app.use((req,res,next) =>{})  
 */

/* pass in a filter (path) as 1st param
to direct to page with routes &
routes import const (route handler) as 2nd param*/ 
app.use('/products', productsRoutes);
app.use('/orders', ordersRoutes);


// Use the middleware (app.use()) to handle errors
app.use((request,response, next)=>{
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error,request,response, next)=>{
    error.status(error.status || 500);
    response.json({
        error:{
            message: error.message
        }
    });
});

// export the app module
module.exports = app;