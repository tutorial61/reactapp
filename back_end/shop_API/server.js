// get the http protocol from node 
const http = require('http');
// import express app
const app = require('./app');

/* get the port from enviroment 
variables, 
or use port 3000 as default*/ 
const port = process.env.PORT  || 3000;

// create an http server 
const server = http.createServer(app);

/* start the server 
&  
make it listen on port */
server.listen(port);