import { Web3Storage } from 'web3.storage'

function getAccessToken () {
  // If you're just testing, you can paste in a token
  // and uncomment the following line:
  // return 'paste-your-token-here'

  // In a real app, it's better to read an access token from an
  // environement variable or other configuration that's kept outside of
  // your code base. For this to work, you need to set the
  // WEB3STORAGE_TOKEN environment variable before you run your code.
//   return process.env.WEB3STORAGE_TOKEN
    return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweEU5ZGRiQ2Q2NjAxYzQwQmI5MjIyMEU4QUUyMUI4ZGUyOTFiYkM3YmUiLCJpc3MiOiJ3ZWIzLXN0b3JhZ2UiLCJpYXQiOjE2ODMzODAxMDgxNjgsIm5hbWUiOiJjaXJ0YWxvZ28ifQ.X3M1zCsLOjq-lhqhaFSv5wMv0BTJUZxjlLXtq41gcVI'
}

function makeStorageClient () {
  return new Web3Storage({ token: getAccessToken() })
}

async function checkStatus (cid) {
    const client = makeStorageClient()
    const status = await client.status(cid)
    console.log(status)
    if (status) {
      // your code to do something fun with the status info here
    }
  }
  
  // insert CID to see info about your upload!

checkStatus('bafybeihigtm4yxmqfzopgp6tqsqw5xsiegilyzh7pi4ekgxaj4x4dy5bqu')